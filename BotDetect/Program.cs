﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CsvHelper;
using GI.Account.Client;
using GI.Account.Client.Interface;
using GI.Account.Client.Model;

namespace BotDetect
{
    class Program
    {
        static async Task Main(string[] args)
        {

            var dateRange = new DateRange
            {
                StartTime = DateTime.UtcNow.AddMinutes(-120),
                EndTime = DateTime.UtcNow
            };


            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) FlashCash PhoneAdmin (Default)");
            Console.WriteLine("2) GPay");
            Console.Write("\r\nSelect an option: ");
            var customerId = "no-operator";
            switch (Console.ReadLine())
            {
                default:
                case "1":
                    customerId = "no-operator";
                    break;
                case "2":
                    customerId = "gpay-no-operator";
                    break;
            }

            //var dateRange = new DateRange
            //{
            //    StartTime = new DateTime(2021, 10, 9, 12, 0, 0),
            //    EndTime = new DateTime(2021, 10, 9, 12, 10, 0),
            //};
            Console.WriteLine($"Looking for IP Auth Attempts between  {dateRange.StartTime.ToLocalTime()} - {dateRange.EndTime.ToLocalTime()} (Local)");
            var badIps = await AnyalizeData(customerId, dateRange);

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu(badIps);
            }


            Console.ReadLine();



        }
        private static bool MainMenu(List<BadIpAddresses> ipaddresses)
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Order By IP Network ID (Default)");
            Console.WriteLine("2) Order Unique Bad Auth Attempts");
            Console.WriteLine("3) Order by Ip Address");
            Console.WriteLine("4) Save to CSV");
            Console.WriteLine("5) Exit");

            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                default:
                case "1":
                    DisplayByIPRange(ipaddresses);
                    return true;
                case "2":
                    DisplayByAttempts(ipaddresses);
                    return true;
                case "3":
                    DisplayByUnquieIP(ipaddresses);
                    return true;
                case "4":
                    SaveToCSV(ipaddresses);
                    return true;
                case "5":
                    return false;
            }
        }
        private static async Task<List<BadIpAddresses>> AnyalizeData(string customerId, DateRange dateRange)
        {
            var _admin = new AccountClient("https://accountservice.gi-web.net");
            var ipAddresses = await _admin.GetAuthLogsByCustomer(customerId, dateRange);
            var badIPs = new List<BadIpAddresses>();
            var distinctIPAddresess = ipAddresses.Select(x => x.IpAddress).Distinct().ToList();

            var q = 1;
            foreach (var ip in distinctIPAddresess)
            {
                Console.Write("\r{0}/{1} IPs Checked", q, distinctIPAddresess.Count());

                var logs = await _admin.GetAuthLogsByIpAddress(ip);
                if (customerId == "no-operator")
                {
                    logs = logs.Where(x => x.System == "flashcash").ToList();
                }
                else
                {
                    logs = logs.Where(x => x.System == "gpay").ToList();
                }
                var sucesses = logs.Where(x => x.Success == true);
                var failure = logs.Where(x => x.Success == false);
                var uniqueAttempts = logs
                                    .Select(x => x.EmailAddress)
                                    .Distinct()
                                    .Count();
                if (((sucesses.Count() / logs.Count()) <= .25) && uniqueAttempts > 25)
                {
                    var ip4 = IPAddress.Parse(ip).MapToIPv4().ToString();
                    var ipMask = ip4.Split(".");
                    var networkId = $"{ipMask[0]}.{ipMask[1]}.{ipMask[2]}.0/24";
                    badIPs.Add(new BadIpAddresses
                    {
                        IpAddress = ip,
                        IpAddressV4 = ip4,
                        Attempts = uniqueAttempts,
                        NetworkId = networkId,
                        Successes = sucesses.Count(),
                        Failures = failure.Count()
                    }
                    );
                }
                q++;
            }
            Console.WriteLine("");
            Console.WriteLine("");

            return badIPs.Distinct().ToList(); ;
        }

        private static void DisplayByAttempts(List<BadIpAddresses> ipaddresses)
        {
            Console.Clear();
            var sortedIps = ipaddresses.OrderByDescending(x => x.Attempts);
            Console.WriteLine("------------------------");
            Console.WriteLine("S: Success");
            Console.WriteLine("F: Failures");
            Console.WriteLine("A: Unique Attempts");
            Console.WriteLine("------------------------");
            Console.WriteLine("");
            Console.WriteLine("Sorted By Attempts:");
            foreach (var i in sortedIps)
            {
                Console.WriteLine($"{i.IpAddress.PadRight(24)} || S:{i.Successes.ToString().PadLeft(4)} F:{i.Failures.ToString().PadLeft(4)} A:{i.Attempts.ToString().PadLeft(4)}");
            }
            Console.WriteLine("");
            Console.WriteLine("------------------------");
            Console.WriteLine("");
            Console.WriteLine($"Total Suspect IP: {sortedIps.Count()}");

            Console.Write("\r\nPress Enter to return to Main Menu");
            Console.ReadLine();
        }

        private static void DisplayByUnquieIP(List<BadIpAddresses> ipaddresses)
        {
            Console.Clear();
            var sortedIps = ipaddresses.OrderByDescending(x => x.IpAddressV4);

            Console.WriteLine("------------------------");
            Console.WriteLine("S: Success");
            Console.WriteLine("F: Failures");
            Console.WriteLine("A: Unique Attempts");
            Console.WriteLine("------------------------");
            Console.WriteLine("");
            Console.WriteLine("Sorted By Attempts:");
            foreach (var i in sortedIps)
            {
                Console.WriteLine($"{i.IpAddress.PadRight(24)} || S:{i.Successes.ToString().PadLeft(4)} F:{i.Failures.ToString().PadLeft(4)} A:{i.Attempts.ToString().PadLeft(4)}");
            }
            Console.WriteLine("");
            Console.WriteLine("------------------------");
            Console.WriteLine("");
            Console.WriteLine($"Total Suspect IP: {sortedIps.Count()}");

            Console.Write("\r\nPress Enter to return to Main Menu");
            Console.ReadLine();
        }
        private static void DisplayByIPRange(List<BadIpAddresses> ipaddresses)
        {
            Console.Clear();
            var sortedIps = ipaddresses.GroupBy(n => n.NetworkId)
                         .Select(n => new
                         {
                             NetworkId = n.Key,
                             Success = n.Sum(c => c.Successes),
                             Failures = n.Sum(c => c.Failures),
                             Attempts = n.Sum(c => c.Attempts),
                             MetricCount = n.Count()
                         })
                         .OrderByDescending(n => n.MetricCount);
            Console.WriteLine("------------------------");
            Console.WriteLine("C: Number of unique IP's in network");
            Console.WriteLine("S: Total Success across IPs in network");
            Console.WriteLine("F: Total Failures across IPs in network");
            Console.WriteLine("A: Total Unique Attempts across IPs in network");
            Console.WriteLine("------------------------");
            Console.WriteLine("");
            Console.WriteLine("Sorted By Attempts:");
            foreach (var i in sortedIps)
            {
                Console.WriteLine($"{i.NetworkId.PadRight(24)} || C:{i.MetricCount.ToString().PadLeft(4)} S:{i.Success.ToString().PadLeft(4)} F:{i.Failures.ToString().PadLeft(4)} A:{i.Attempts.ToString().PadLeft(4)}");
            }
            Console.WriteLine("");
            Console.WriteLine("------------------------");
            Console.WriteLine("");
            Console.WriteLine($"Total Suspect Networks: {sortedIps.Count()}");

            Console.Write("\r\nPress Enter to return to Main Menu");
            Console.ReadLine();
        }

        private static void SaveToCSV(List<BadIpAddresses> ipaddresses)
        {
            Console.Clear();
            var sortedIps = ipaddresses.OrderByDescending(x => x.IpAddressV4).ToList();
            string filePath = @$"c:\temp\badIps-{DateTime.UtcNow.Ticks}.csv";
            using (var writer = new StreamWriter(filePath))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(sortedIps);
            }

            Console.WriteLine($"File Written to {filePath}");           
            Console.WriteLine();

            Console.Write("\r\nPress Enter to return to Main Menu");
            Console.ReadLine();
        }
        public class BadIpAddresses
        {
            public string IpAddress { get; set; }
            public string IpAddressV4 { get; set; }
            public string NetworkId { get; set; }
            public int Attempts { get; set; }
            public int Successes { get; set; }
            public int Failures { get; set; }

        }

    }
}
